﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Timer_Windows.Classes;

namespace Timer_Windows
{
    /// <summary>
    /// Interaction logic for IpSelectingWindow.xaml
    /// </summary>
    public partial class IpSelectingWindow : Window
    {
        public IpSelectingWindow(List<string> ipItems)
        {
            InitializeComponent();
            ipItem1TextBlock.Text = ipItems[0];
            for (int i = 1; i < ipItems.Count; i++)
            {
                ipItemsContainerStackPanel.Children.Add(DeepCopyAGrid(ipItem1, String.Format("{0}{1}", IP_ITEM_NAME, (i + 1)), ipItems[i]));
            }
        }

        #region METÓDUSOK
        private Grid DeepCopyAGrid(Grid copyAbleGrid, string newGridName, string newIp)
        {
            Grid copiedGrid = XamlReader.Parse(XamlWriter.Save(copyAbleGrid)) as Grid;
            copiedGrid.Name = newGridName;

            RadioButton ipRadioButton = ((copiedGrid.Children[0] as Border).Child as RadioButton);
            ipRadioButton.Name = copiedGrid.Name + RADIO_BUTTON_NAME;
            ipRadioButton.Checked += ipItemRadioButton_Checked;

            TextBlock ipTextBlock = ((copiedGrid.Children[1] as Border).Child as TextBlock);
            ipTextBlock.Name = copiedGrid.Name + TEXT_BLOCK_NAME;
            ipTextBlock.Text = newIp;

            return copiedGrid;
        }

        private void ipItemRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            //Debug.WriteLine(radioButton.Name.Split(new string[] { "ipItem" }, StringSplitOptions.None)[1].Split(new string[] { "RadioButton" },StringSplitOptions.None)[0]);
            SelectedIp = int.Parse(radioButton.Name.Split(new string[] { IP_ITEM_NAME }, StringSplitOptions.None)[1].Split(new string[] { RADIO_BUTTON_NAME }, StringSplitOptions.None)[0]);
        }

        private void ipSelectingOkButton_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedIp > 0)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show(StringsResxManager.GetOneIpSelectedNeedMessage());
            }
        }
        #endregion

        #region MEZŐK
        private int selectedIp = 0;
        private const string IP_ITEM_NAME = "ipItem";
        private const string RADIO_BUTTON_NAME = "RadioButton";
        private const string TEXT_BLOCK_NAME = "TextBlock";

        public int SelectedIp
        {
            get { return selectedIp; }
            private set { selectedIp = value; }
        }
        #endregion
    }
}
