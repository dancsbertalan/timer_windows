﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using Timer_Windows.Classes;

namespace Timer_Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region MEZŐK
        UserUi user;
        RemoteManager remoteManager;
        #endregion

        #region KONSTRUKTOROK
        public MainWindow()
        {
            InitializeComponent();
            user = UserUi.GetInstance();
            remoteManager = RemoteManager.GetInstance();
            this.DataContext = user;
            TimerManager.Initialize();
            List<string> ipItems = new List<string>(new string[] { "192.168.1.12", "192.168.1.13", "192.168.1.14", "192.168.1.15" });

        }
        #endregion  

        #region METÓDUSOK
        private void actionClickHandler(string executableAction)
        {

            if (user.ActualAction == StringsResxManager.GetNoAction())
            {
                user.ActualAction = executableAction;
                TimerManager.StartTimer(hourTextBox.Text, minuteTextBox.Text, false);
            }
            else if (user.ActualAction == executableAction)
            {
                TimerManager.StopTimer();
            }
            else
            {
                MessageBox.Show(String.Format(StringsResxManager.GetPleaseStopSelectedAction(), user.ActualAction));
            }
        }
        #endregion

        private void shutdownButton_Click(object sender, RoutedEventArgs e)
        {
            actionClickHandler(StringsResxManager.GetShutdownButton());
        }

        private void rebootButton_Click(object sender, RoutedEventArgs e)
        {
            actionClickHandler(StringsResxManager.GetRebootButton());
        }

        private void sleepButton_Click(object sender, RoutedEventArgs e)
        {
            actionClickHandler(StringsResxManager.GetSleepButton());
        }

        private void logoutButton_Click(object sender, RoutedEventArgs e)
        {
            actionClickHandler(StringsResxManager.GetLogoutButton());
        }

        private void lockButton_Click(object sender, RoutedEventArgs e)
        {
            actionClickHandler(StringsResxManager.GetLockButton());
        }

        private void hibernateButton_Click(object sender, RoutedEventArgs e)
        {
            actionClickHandler(StringsResxManager.GetHibernateButton());
        }

        private void hourTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Consts.OnlyNumber(hourTextBox);
            Consts.MinimumNullNumber(hourTextBox);
            Consts.FirstNullDelete(hourTextBox);
        }

        private void minuteTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Consts.OnlyNumber(minuteTextBox);
            Consts.MinimumNullNumber(minuteTextBox);
            Consts.FirstNullDelete(minuteTextBox);
        }

        private void remoteMenuItem_Checked(object sender, RoutedEventArgs e)
        {
            remoteManager.StartServer();
        }

        private void remoteMenuItem_Unchecked(object sender, RoutedEventArgs e)
        {
            remoteManager.StopServer();
        }
    }
}
