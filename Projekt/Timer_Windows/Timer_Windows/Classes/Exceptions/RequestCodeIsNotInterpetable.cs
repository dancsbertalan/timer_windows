﻿using System;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// Egy olyan speciális kivételt kezel le amikor az adott requestben a code nem értelmezhető.
    /// Az az ebben a requestben nincs olyan lehetőség ahol ez a code kellene.
    /// </summary>
    class RequestCodeIsNotInterpetable : Exception
    {
        /// <summary>
        /// Inicializálja a változókat melyek alapján majd a Message property készül.
        /// </summary>
        /// <param name="request">A kérés neve</param>
        /// <param name="code">A kód amely nem értelmezhető a request paraméterben</param>
        public RequestCodeIsNotInterpetable(string request,int code)
        {
            this.request = request;
            this.code = code;
        }

        /// <summary>
        /// Felparaméterezve vissza adja a xy_request_xy_code_is_not_interpetable üzentet amelyet a responsenál tudunk felhasználni.
        /// </summary>
        public override string Message
        {
            get
            {
                return String.Format(MessageConsts.KEY_REQUEST_CODE_IS_NOT_INTERPETABLE_MESSAGE,request,code);
            }
        }

        string request;
        int code;
    }
}
