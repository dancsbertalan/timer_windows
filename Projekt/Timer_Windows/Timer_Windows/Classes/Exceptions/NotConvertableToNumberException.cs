﻿using System;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// Egy olyan "speciáls" kivétel melyet azokban az esetekben használunk amikor egy stringként kapott számot nem tudunk konvertálni int-be. (Használható más szám változókra is!)
    /// </summary>
    class NotConvertableToNumberException : Exception
    {
        /// <summary>
        /// Inicializálja a változókat.
        /// </summary>
        /// <param name="field">Mező neve</param>
        /// <param name="value">Mező értéke</param>
        /// <param name="request">Kérés neve (amennyiben volt)</param>
        public NotConvertableToNumberException(string field, string value , string request = null)
        {
            this.field = field;
            this.value = value;
            this.request = request;
        }

        /// <summary>
        /// A "megjelenítendő" üzenetet adja vissza. Két különböző kimenetele van.
        /// 1. - xy_request_xy_field_xy_value_not_convertable_to_number_message üzentet adja vissza amelyet a responsenál (válaszok) használhatunk.
        /// 2. - Az adott nyelvi beállításokhoz viszonyítva megkapjuk a "notConvertableToNumberExepctionMessage" kulcs alá tartozó üzentet. 
        /// </summary>
        public override string Message
        {
            get
            {
                string message = string.Empty;

                if (request != null)
                {
                    message = String.Format(MessageConsts.KEY_REQUEST_FIELD_VALUE_NOT_CONVERTABLE_TO_NUMBER_MESSAGE, request, field, value);
                }
                else
                {
                    message = String.Format(StringsResxManager.GetNotConvertableToNumberExepctionMessage(), field, value);
                }

                return message;
            }
        }


        private string field;
        private string value;
        private string request;

    }
}
