﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timer_Windows.Classes.Exceptions
{
    class InvalidBodyFormException : Exception
    {
        public InvalidBodyFormException(string body)
        {
            this.body = body;
        }

        public string Message
        {
            get { return MessageConsts.KEY_BODY_FORM_IS_INVALID_CHECK_DOCUMENTATION_MESSAGE; }
        }

        string body;

        public Dictionary<string, object> ResponseFormat
        {
            get
            {
                Dictionary<string, object> response = new Dictionary<string, object>();

                response.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_BODY_FORM_IS_INVALID_CHECK_DOCUMENTATION_CODE);
                response.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_BODY_FORM_IS_INVALID_CHECK_DOCUMENTATION_MESSAGE);
                response.Add(FieldConsts.KEY_BODY_FIELD, body);

                return response;
            }
        }

    }
}
