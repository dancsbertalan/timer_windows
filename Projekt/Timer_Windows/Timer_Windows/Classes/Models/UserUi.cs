﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Net;
using System.Diagnostics;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// Ez az osztály modellezi le a prgram számára a felhasználónkat.
    /// Hozzá rendeljük az Ő beállításait az alapján ahogyan eltároltuk azt az erre a célre létrehozott és kezelt fájlban. (SQLite file)
    /// </summary>
    class UserUi : INotifyPropertyChanged
    {
        #region KONSTRUKTOROK
        /// <summary>
        /// Inicializálja a felhasználó beállításait az eltároltak alapján.
        /// </summary>
        private UserUi()
        {
            //Adatbázishoz való nyulkálás
            //TODO: 1 - A user-t az alapján kiszedni amelyiket majd a felhasználó választja.
            int selectedUserId = 1;
            User = ((SettingsDatabase.GetInstance()) as SettingsDatabase).GetUserById(selectedUserId);
        }
        #endregion

        #region MEZŐK
        private static UserUi instance;
        private string actualAction;
        private string remainingTime = "00:00";

        private List<string> localIps;
        private int currentLocalIpIndex = 0;
        private string localIp = StringsResxManager.GetNoRemote();
        private string remainingMinute = "0";
        private string remainingHour = "0";

        public event PropertyChangedEventHandler PropertyChanged;

        private User user;
        #endregion

        #region PROPERTYK
        /// <summary>
        /// A UserUi modellnek modelnek a user példánya amely az adatbázissal való komunikációt intézi.
        /// Ezen keresztül szedjük ki a beállításokat. Módosíthatjuk a beállításokat. És azok fel is kerülnek az adatbázisba.
        /// </summary>
        public User User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        /// <summary>
        /// A felhasználónak az aktuálisan kiválasztott műveletét adja vissza.
        /// Amennyiben nincs kiválasztva semmi akkor: 
        /// </summary>
        public string ActualAction
        {
            get
            {
                if (actualAction == null)
                {
                    actualAction = StringsResxManager.GetNoAction();
                }
                return actualAction;
            }
            set
            {
                actualAction = value;
                OnNotifyPropertyChanged();
            }
        }

        /// <summary>
        /// A hátralevő idő az adott műveletig. Csak az órát és a percet adja meg.
        /// </summary>
        public string RemainingTime
        {
            get
            {
                return remainingTime;
            }
            set
            {
                remainingTime = value;
                RemainingMinute = int.Parse(RemainingTime.Split(':')[1]).ToString();
                RemainingHour = int.Parse(RemainingTime.Split(':')[0]).ToString();
                OnNotifyPropertyChanged();
            }
        }

        /// <summary>
        /// A jelenlegi futtatási környezetben adja meg a local ip-t. (Az adott hálózatra csatlakoztatva)
        /// </summary>
        private List<string> LocalIps
        {
            get
            {
                return localIps;
            }
            set
            {
                localIps = value;
            }
        }

        /// <summary>
        /// Amennyiben több hálózaton is fennt van a felhasználó (wifi / lan) ebben tároljuk, hogy melyiket akarja használni, mint lokális hálózati ip.
        /// </summary>
        private int CurrentLocalIpIndex
        {
            get
            {
                return currentLocalIpIndex;
            }
            set
            {
                currentLocalIpIndex = value;
            }
        }

        /// <summary>
        /// Ha több local ip van akkor a megfelelően kiválasztottat tartalmazza. Amennyiben pedig csak egy darab van akkor csak azt az egyet adja vissza, melyet a SetLocalIp() beállított.
        /// Ha pedig nincsen akkor "null".
        /// </summary>
        public string LocalIp
        {
            get
            {
                return localIp;
            }
            private set
            {
                localIp = value;
                OnNotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Hátralevő percek száma.
        /// </summary>
        public string RemainingMinute
        {
            get
            {
                return remainingMinute;
            }
            set
            {
                remainingMinute = value;
                OnNotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Hátralevő órák száma.
        /// </summary>
        public string RemainingHour
        {
            get
            {
                return remainingHour;
            }
            set
            {
                remainingHour = value;
                OnNotifyPropertyChanged();
            }
        }

        #endregion

        #region METÓDUSOK
        /// <summary>
        /// Singleton tervezési minta alapján mindig ugyan azt a példányt adja vissza.
        /// </summary>
        /// <returns></returns>
        public static UserUi GetInstance()
        {
            if (instance == null)
            {
                instance = new UserUi();
            }
            return instance;
        }

        /// <summary>
        /// Beállítja a localip-t annak alapján, hogy melyiket választjuk.
        /// 
        ///     Kivételek:
        ///         WebException
        /// 
        /// </summary>
        public void SetLocalIp()
        {
            try
            {
                localIps = Consts.GetLocalIpAddresses();
            }
            catch (WebException webE)
            {
                throw webE;
            }

            if (localIps.Count > 1)
            {
                //TODO: 0 - Akkor kell választási lehetőség, hogy melyiket használjuk!
                CurrentLocalIpIndex = WindowManager.ShowIpSelectWindow(LocalIps);
                LocalIp = localIps[CurrentLocalIpIndex];
            }
            else if (localIps.Count == 1)
            {
                LocalIp = localIps[0];
            }
            else if (localIps.Count <= 0)
            {
                LocalIp = string.Empty;
                User.Settings.RemoteIsOn = false;
            }
        }

        /// <summary>
        /// A local ip-t nullázza.
        /// </summary>
        public void UnSetLocalIp()
        {
            LocalIp = StringsResxManager.GetNoRemote();
            //OnNotifyPropertyChanged(nameof(LocalIp));
        }

        /// <summary>
        /// Az implementációja az INotifyPropertyChanged interface-nek.
        /// Ha egy adott property-nél szeretnénk azt, hogy frissüljön a felhasználói felületen az adat akkor ezt kell a setter metódusban a változó új értékének megkapása utána kell elhelyezni.
        /// </summary>
        /// <param name="propertyName">A hívó property neve - Nem kell megadni a hívásánál automatikusan a kód tudja, hogy mi volt a hívója. Amennyiben ez nem úgy történik akkor direktbe beírható.</param>
        public void OnNotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
        #endregion


    }
}
