﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// A felhasználó beállításait tartalmazó osztály.
    /// </summary>
    class Settings : INotifyPropertyChanged
    {
        /// <summary>
        /// Inicializálja a beállításokat
        /// </summary>
        /// <param name="id">A beállítás egyedi azonosítója</param>
        /// <param name="remoteIsOn">A távirányító be van-e kapcsolva.</param>
        public Settings(int id, byte remoteIsOn)
        {
            Id = id;
            RemoteIsOn = Convert.ToBoolean(remoteIsOn);
            isInicialized = true;
        }

        /// <summary>
        /// Vissza adja a settings példányt string formában.
        /// </summary>
        /// <returns type="string">Settings: Id: 1, RemoteIsOn: false</returns>
        public override string ToString()
        {
            return String.Format("Settings: Id: {0}, RemoteIsOn: {1}", Id, RemoteIsOn);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        /// <summary>
        /// Az implementációja az INotifyPropertyChanged interface-nek.
        /// Ha egy adott property-nél szeretnénk azt, hogy frissüljön a felhasználói felületen az adat akkor ezt kell a setter metódusban a változó új értékének megkapása utána kell elhelyezni.
        /// </summary>
        /// <param name="propertyName">A hívó property neve - Nem kell megadni a hívásánál automatikusan a kód tudja, hogy mi volt a hívója. Amennyiben ez nem úgy történik akkor direktbe beírható.</param>
        public void OnNotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventArgs e = new PropertyChangedEventArgs(propertyName);
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private int id;
        /// <summary>
        /// Egyedi azonosító
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        //0 - false || 1 - true
        private bool remoteIsOn;
        /// <summary>
        /// A távirányító bekapcsoltsága
        /// </summary>
        public bool RemoteIsOn
        {
            get { return remoteIsOn; }
            set
            {
                remoteIsOn = value;
                OnNotifyPropertyChanged();
                if (isInicialized)
                {
                    (SettingsDatabase.GetInstance() as SettingsDatabase).UpdateRemoteIsOnBySettings(this);
                }
            }
        }

        private bool isInicialized = false;

    }
}
