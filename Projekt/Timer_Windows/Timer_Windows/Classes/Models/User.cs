﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// User modell az adatbázis alapján
    /// </summary>
    class User
    {
        /// <summary>
        /// Inicializálja a usert.
        /// </summary>
        /// <param name="id">A felhasználó egyedi azonosítója</param>
        /// <param name="name">A felhasználó neve</param>
        /// <param name="settings">A felhasználó beállításai</param>
        public User(int id, Settings settings)
        {
            Id = id;
            Settings = settings;
        }

        /// <summary>
        /// Vissza adja a User-t string formában a beállításaival együtt
        /// </summary>
        /// <returns type="string">User: Id: 1, Name: Example, Settings: Settings: 1, false</returns>
        public override string ToString()
        {
            return String.Format("User: Id: {0}, Settings: {1}", id, settings.ToString());
        }

        private int id;
        /// <summary>
        /// Egyedi azonosító
        /// </summary>
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
                
        private Settings settings;
        /// <summary>
        /// Beállítások
        /// </summary>
        public Settings Settings
        {
            get { return settings; }
            set { settings = value; }
        }

    }
}
