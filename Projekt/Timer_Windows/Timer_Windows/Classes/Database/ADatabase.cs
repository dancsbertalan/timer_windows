﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.SQLite;

namespace Timer_Windows.Classes
{
    abstract class ADatabase
    {
        #region KONSTRUKTOROK
        protected ADatabase(string databaseName)
        {
            directory = String.Format("{0}{1}\\", AppDomain.CurrentDomain.BaseDirectory, DATABASE_FOLDER);
            sourceUrl = String.Format("{0}{1}.db", directory, databaseName);

            sqliteConnection = new SQLiteConnection(String.Format(BASIC_CONNECTION_STRING, sourceUrl));
            sqliteCommand = new SQLiteCommand(sqliteConnection);
        }
        #endregion

        #region METÓDUSOK
        protected void CloseConnect()
        {
            if (!sqliteDataReader.IsClosed)
            {
                sqliteDataReader.Close();
            }
            sqliteConnection.Close();
        }

        protected abstract void Connect();

        protected abstract void CreateTables();
        #endregion

        #region MEZŐK
        protected SQLiteConnection sqliteConnection;
        protected SQLiteDataReader sqliteDataReader;
        protected SQLiteCommand sqliteCommand;
        protected static ADatabase instance;
        protected static string sourceUrl;
        protected static string directory;
        private const string BASIC_CONNECTION_STRING = "Data Source={0};Version=3;";
        private const string DATABASE_FOLDER = "Databases";
        #endregion
    }
}
