﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using System.Windows;
using System.Diagnostics;

namespace Timer_Windows.Classes
{
    class SettingsDatabase : ADatabase
    {
        #region KONSTRUKTOROK
        private SettingsDatabase(string databaseName)
            : base(databaseName)
        {

        }
        #endregion

        #region METÓDUSOK
        /// <summary>
        /// Vissza adja egy példányát a SettingsDatabasenek az ADatabase-n keresztül
        /// </summary>
        /// <returns type="ADatabase"></returns>
        public static ADatabase GetInstance()
        {
            if (instance == null)
            {
                instance = new SettingsDatabase(DATABASE_NAME);
            }
            return instance;
        }

        /// <summary>
        /// A csatlakozásért felelős metódus.
        /// </summary>
        protected override void Connect()
        {
            try
            {
                bool isFileExistsBeforeOpen = File.Exists(sourceUrl);
                sqliteConnection.Open();
                if (!isFileExistsBeforeOpen)
                {
                    CreateTables();
                }
            }
            catch (SQLiteException)
            {
                Directory.CreateDirectory(directory);
                sqliteConnection.Open();
                CreateTables();
            }
        }

        /// <summary>
        /// Elkészíti a táblákat a megfelelő kapcsolatokkal.
        /// </summary>
        protected override void CreateTables()
        {
            sqliteCommand.CommandText = String.Format("create table `{0}` (`{1}` integer not null primary key autoincrement unique," +
                " `{2}` integer not null  default 0)", SETTINGS_TABLE, ID_FIELD, REMOTE_IS_ON_FIELD);
            sqliteCommand.ExecuteNonQuery();

            sqliteCommand.CommandText = String.Format("create table `{0}` (`{1}` integer not null primary key autoincrement unique," +
                "`{2}` integer not null," +
                "foreign key(`{2}`) references `{3}`(`{1}`))", USER_TABLE, ID_FIELD, SETTINGS_ID_FIELD, SETTINGS_TABLE);
            sqliteCommand.ExecuteNonQuery();

            sqliteCommand.CommandText = String.Format("insert into {0} ({1},{2}) values ({3},{4})", SETTINGS_TABLE, ID_FIELD, REMOTE_IS_ON_FIELD, INIT_ID_VALUE, INIT_REMOTE_IS_ON_VALUE);
            sqliteCommand.ExecuteNonQuery();

            sqliteCommand.CommandText = String.Format("insert into {0} ({1},{2}) values ({3},{4})", USER_TABLE, ID_FIELD, SETTINGS_ID_FIELD, INIT_ID_VALUE, INIT_ID_VALUE);
            sqliteCommand.ExecuteNonQuery();

            sqliteCommand.CommandText = "";

        }

        /// <summary>
        /// Vissza adja a usert a paraméterben megadott ID alapján.
        /// </summary>
        /// <param name="userId">A kért felhasználó ID-ja</param>
        /// <returns type="User">A User egy példánya</returns>
        public User GetUserById(int userId)
        {
            Connect();
            User user = null;

            sqliteCommand.CommandText = String.Format("select {0}.{1}, {0}.{2}, {3}.{1}, {3}.{4} from {0} inner join {3}" +
                " on {0}.{2} = {3}.{1}" +
                " where {0}.{1} = {5}", USER_TABLE, ID_FIELD, SETTINGS_ID_FIELD, SETTINGS_TABLE, REMOTE_IS_ON_FIELD, userId);
            sqliteDataReader = sqliteCommand.ExecuteReader();

            int userFieldId = int.MaxValue;
            string userFieldName = string.Empty;
            int userFieldSettingsId = int.MaxValue;
            int settingsFieldId = int.MaxValue;
            byte settingsFieldRemoteIsOn = byte.MaxValue;

            while (sqliteDataReader.Read())
            {
                userFieldId = (int)sqliteDataReader.GetInt64(0);
                userFieldSettingsId = (int)sqliteDataReader.GetInt64(1);
                settingsFieldId = (int)sqliteDataReader.GetInt64(2);
                settingsFieldRemoteIsOn = (byte)sqliteDataReader.GetInt64(3);

            }

            CloseConnect();

            Settings userSettings = new Settings(settingsFieldId, settingsFieldRemoteIsOn);
            user = new User(userFieldId, userSettings);


            Debug.WriteLine(user);

            return user;
        }

        public void UpdateRemoteIsOnBySettings(Settings settings)
        {
            Connect();

            sqliteCommand.CommandText = String.Format("update {0} set {1} = {2} where {3} = {4}", SETTINGS_TABLE, REMOTE_IS_ON_FIELD, Convert.ToByte(settings.RemoteIsOn), ID_FIELD, settings.Id);
            sqliteCommand.ExecuteNonQuery();

            CloseConnect();
        }

        #endregion

        #region MEZŐK
        private const string DATABASE_NAME = "Settings";

        private const string USER_TABLE = "User";
        private const string SETTINGS_TABLE = "Settings";

        private const string ID_FIELD = "id";
        private const string AGE_FIELD = "age";
        private const string SETTINGS_ID_FIELD = "settingsId";

        private const string REMOTE_IS_ON_FIELD = "remoteIsOn";

        private const int INIT_ID_VALUE = 1;
        private const byte INIT_REMOTE_IS_ON_VALUE = 0;
        #endregion
    }
}
