﻿
namespace Timer_Windows.Classes
{
    /// <summary>
    /// A requestekhnél fellelhető összes mezőt tartalmazó osztály.
    /// </summary>
    public static class FieldConsts
    {
        public const string KEY_DATA_FIELD = "data";
        public const string KEY_MESSAGE_FIELD = "message";
        public const string KEY_CODE_FIELD = "code";
        public const string KEY_HOUR_FIELD = "hour";
        public const string KEY_MINUTE_FIELD = "minute";

        public const string KEY_BODY_FIELD = "body";
    }
}
