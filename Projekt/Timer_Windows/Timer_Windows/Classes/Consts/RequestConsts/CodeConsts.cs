﻿using System.Collections.Generic;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// A requestekhez tartozó kódokat tartalmazó osztály.
    /// </summary>
    public static class CodeConsts
    {
        public const int KEY_ACTUAL_ACTION_NO_CODE = 601;
        public const int KEY_ACTUAL_ACTION_SHUTDOWN_CODE = 602;
        public const int KEY_ACTUAL_ACTION_LOCK_CODE = 603;
        public const int KEY_ACTUAL_ACTION_LOGOUT_CODE = 604;
        public const int KEY_ACTUAL_ACTION_REBOOT_CODE = 605;
        public const int KEY_ACTUAL_ACTION_SLEEP_CODE = 606;
        public const int KEY_ACTUAL_ACTION_HIBERNATE_CODE = 619;
        public const int KEY_YOUR_SELECTED_ACTION_STARTING_IS_SUCCESS_CODE = 607;
        public const int KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_CODE = 608;
        public const int KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_CODE = 609;
        public const int KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_CODE = 610;
        public const int KEY_REQUEST_DOES_NOT_EXIST_IN_GET_ARRAY_CODE = 611;
        public const int KEY_REQUEST_DOES_NOT_EXIST_IN_POST_ARRAY_CODE = 612;
        public const int KEY_REQUEST_DOES_NOT_EXIST_CODE = 613;
        public const int KEY_ONE_OR_MORE_PARAMETERS_ARE_MISSING_CODE = 614;
        public const int KEY_REQUEST_CODE_FIELD_VALUE_IS_NOT_CONVERTABLE_TO_NUMBER_CODE = 615;
        public const int KEY_REQUEST_CODE_IS_NOT_INTERPETABLE_CODE = 616;
        public const int KEY_REQUEST_FIELD_VALUE_IS_NOT_CONVERTABLE_TO_NUMBER_CODE = 617;
        public const int KEY_BODY_FORM_IS_INVALID_CHECK_DOCUMENTATION_CODE = 619;

        /// <summary>
        /// Az érvényes kódokat tartalmazza a set_timer requesthez. Így egyszerűbben lehet ellenőrizni azt, hogy értelmezhető-e az adott kód ennél a requestnél.
        /// </summary>
        public static List<int> validSetTimerCodes = new List<int>(new int[]
        {
            KEY_ACTUAL_ACTION_SHUTDOWN_CODE,
            KEY_ACTUAL_ACTION_LOCK_CODE,
            KEY_ACTUAL_ACTION_LOGOUT_CODE,
            KEY_ACTUAL_ACTION_REBOOT_CODE,
            KEY_ACTUAL_ACTION_SLEEP_CODE,
            KEY_ACTUAL_ACTION_HIBERNATE_CODE
        });
    }
}
