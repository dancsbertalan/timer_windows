﻿using System.Collections.Generic;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// Az összes request kulcsot tartalmazó osztály.
    /// </summary>
    public static class RequestConsts
    {
        public const string KEY_ACTUAL_ACTION_GET_REQUEST = "actual_action";
        public const string KEY_SET_TIMER_POST_REQUEST = "set_timer";
        public const string KEY_STOP_ACTUAL_ACTION_POST_REQUEST = "stop_actual_action";

        /// <summary>
        /// Minden post requestet tartalmaz amelyet a szerver kaphat. Így egyszerűbben lehet ellenőrizni azt, hogy létezik-e az adott kérés a post tömbben.
        /// </summary>
        public static List<string> postRequests = new List<string>(new string[]
        {
            KEY_SET_TIMER_POST_REQUEST,
            KEY_STOP_ACTUAL_ACTION_POST_REQUEST
        });

        /// <summary>
        /// Minden get requestet tartalmaz amelyet a szerver kaphat. Így egyszerűbben lehet ellenőrizni azt, hogy létezik-e az adott kérés a get tömbben.
        /// </summary>
        public static List<string> getRequests = new List<string>(new string[]
        {
            KEY_ACTUAL_ACTION_GET_REQUEST
        });
    }
}
