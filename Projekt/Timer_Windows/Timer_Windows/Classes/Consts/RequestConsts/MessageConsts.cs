﻿
namespace Timer_Windows.Classes
{
    /// <summary>
    /// A requestekhez (response) kellő oszály message field értéke itt található meg.
    /// </summary>
    public static class MessageConsts
    {
        public const string KEY_NO_ACTUAL_ACTION_MESSAGE = "no_actual_action";
        public const string KEY_ACTUAL_ACTION_SHUTDOWN_MESSAGE = "shutdown";
        public const string KEY_ACTUAL_ACTION_LOCK_MESSAGE = "lock";
        public const string KEY_ACTUAL_ACTION_LOGOUT_MESSAGE = "logout";
        public const string KEY_ACTUAL_ACTION_REBOOT_MESSAGE = "reboot";
        public const string KEY_ACTUAL_ACTION_SLEEP_MESSAGE = "sleep";
        public const string KEY_ACTUAL_ACTION_HIBERNATE_MESSAGE = "hibernate";
        public const string KEY_YOUR_SELECTED_ACTION_STARTING_IS_SUCCESS_MESSAGE = "your_selected_action_starting_is_success";
        public const string KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_MESSAGE = "your_selected_action_starting_is_failed";
        public const string KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_MESSAGE = "actual_action_stopping_is_success";
        public const string KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_MESSAGE = "actual_action_stopping_is_failed";
        public const string KEY_ONE_OR_MORE_PARAMETERS_ARE_MISSING_MESSAGE = "one_or_more_parameters_are_missing";
        public const string KEY_REQUEST_CODE_IS_NOT_INTERPETABLE_MESSAGE = "{0}_request_{1}_code_is_not_interpetable";
        public const string KEY_REQUEST_FIELD_VALUE_NOT_CONVERTABLE_TO_NUMBER_MESSAGE = "{0}_request_{1}_field_value_{2}_not_convertable_to_number";
        public const string KEY_REQUEST_DOES_NOT_EXIST_MESSAGE = "{0}_request_does_not_exist";
        public const string KEY_REQUEST_DOES_NOT_EXIST_IN_ARRAY_MESSAGE = KEY_REQUEST_DOES_NOT_EXIST_MESSAGE + "_in_{1}_array";
        public const string KEY_BODY_FORM_IS_INVALID_CHECK_DOCUMENTATION_MESSAGE = "body_form_is_invalid_check_documentation";
    }
}
