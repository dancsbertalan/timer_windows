﻿using System.Collections.Generic;
using System.Net;
using System.Web.Script.Serialization;
using System.Windows.Controls;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// Minden olyan konstans adatot tartalmaz amelyet egyátalán nem szándékozunk dinamikusan változtatni.
    /// Ilyenek példáúl az elérési utak fájlokhoz, kulcsok és az altalánosság körébe tartoznak.
    /// </summary>
    public static class Consts
    {
        /// <summary>
        /// Csak a számokat engedélyezi az adott textboxban. Ami nem szám azt nem engedi beírni.
        /// </summary>
        /// <param name="textBox">A textBox melyben dolgozni kell.</param>
        public static void OnlyNumber(TextBox textBox)
        {
            string text = textBox.Text;
            int number;
            string newText = "";
            for (int i = 0; i < text.Length; i++)
            {
                if (int.TryParse(text[i].ToString(), out number))
                {
                    newText += number.ToString();
                }
            }
            textBox.Text = newText;
        }

        /// <summary>
        /// Legalább "0" számot tartalmaznia kell a textBox-nak, amennyiben ezt nem tartalmazza beírja.
        /// </summary>
        /// <param name="textBox">Az adott textBox amelyben dolgozni kell.</param>
        public static void MinimumNullNumber(TextBox textBox)
        {
            if (textBox.Text == null || textBox.Text == string.Empty || textBox.Text == "")
            {
                textBox.Text = "0";
            }
        }

        /// <summary>
        /// A szám elejéről a fölösleges nullát törli.
        /// </summary>
        /// <param name="textBox">Az adott textBox amelyben dolgoznunk kell.</param>
        public static void FirstNullDelete(TextBox textBox)
        {
            string text = textBox.Text;
            if (text[0] == '0' && text.Length > 1)
            {
                string newText = "";
                for (int i = 1; i < text.Length; i++)
                {
                    newText += text[i];
                }
                textBox.Text = newText;
            }
        }
        
        /// <summary>
        /// Vissza kapjuk, hogy van-e internet.
        /// 
        ///     Kivételek:
        ///         WebException
        /// 
        /// </summary>
        /// <returns type="bool"></returns>
        public static bool IsHaveInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (var stream = client.OpenRead(GOOGLE_URL))
                {
                    return true;
                }
            }
            catch (WebException webE)
            {
                throw webE;
            }
        }

        /// <summary>
        /// Objectként megkapja azt a változót amelyből json-t kell készíteni.
        /// </summary>
        /// <param name="obj">A json formára konvertálandó object.</param>
        /// <returns type="string">A json formára konvertált obj.</returns>
        public static string JsonObjectSerialize(object obj)
        {
            return new JavaScriptSerializer().Serialize(obj);
        }

        //TODO: 0 - Ha több local ip van melyiket használjuk ? 
        /// <summary>
        ///     Kivételek:
        ///         WebException
        /// </summary>
        /// <returns type="List<string>" name="localIps">A local ipket tartalmazó lista.</returns>
        public static List<string> GetLocalIpAddresses()
        {
            try
            {
                List<string> localIps = new List<string>();
                if (IsHaveInternetConnection())
                {
                    var host = Dns.GetHostEntry(Dns.GetHostName());
                    foreach (var ip in host.AddressList)
                    {
                        if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            localIps.Add(ip.ToString());
                        }
                    }
                }
                return localIps;
            }
            catch (WebException webE)
            {
                throw webE;
            }
        }

        /// <summary>
        /// Szám konvertáló. (Kivétel dobással!)
        /// 
        ///     Kivétel:
        ///         NotConvertableToNumberException
        /// </summary>
        /// <param name="number">A konvertálandó szám string formában</param>
        /// <param name="retNumber">A változó amelyben viszont szeretnénk látni int formában a number paraméter alá tartozó értéket</param>
        /// <param name="field">A field neve amiből számot kellene csinálni.</param>
        /// <param name="request">Amennyiben request-nél konvertálunk a request neve.</param>
        public static void numberConverter(string number, out int retNumber, string field, string request = null)
        {
            retNumber = 0;

            bool isSuccessConversion = int.TryParse(number, out retNumber);

            if (!isSuccessConversion)
            {
                throw new NotConvertableToNumberException(field, number, request);
            }
        }
        
        #region REQUEST KULCSOK        
        public const string KEY_GET_ARRAY_NAME = "GET";
        public const string KEY_POST_ARRAY_NAME = "POST";
        #endregion

        public const int SERVER_PORT = 7171;
        public const string GOOGLE_URL = "http://www.google.com";

        #region EXCEPTION ERROR KÓDOK
        public const int HTTP_PERMISSION_DENIED = 5;
        #endregion
    }
}
