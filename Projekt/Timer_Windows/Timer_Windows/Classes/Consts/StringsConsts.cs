﻿
namespace Timer_Windows.Classes
{
    /// <summary>
    /// Minden strings kulcsot tartalmazó osztály.
    /// </summary>
    public static class StringsConsts
    {
        public const string KEY_STRINGS = "Timer_Windows.Resources.Strings.strings";
        public const string KEY_NO_ACTION_TEXT_BLOCK = "noActionTextBlock";
        public const string KEY_NO_REMOTEABLE_TEXT_BLOCK = "noRemotableTextBlock";
        public const string KEY_LOCK_BUTTON = "lockButton";
        public const string KEY_LOGOUT_BUTTON = "logoutButton";
        public const string KEY_REBOOT_BUTTON = "rebootButton";
        public const string KEY_SHUTDOWN_BUTTON = "shutdownButton";
        public const string KEY_SLEEP_BUTTON = "sleepButton";
        public const string KEY_HIBERNATE_BUTTON = "hibernateButton";
        public const string KEY_PLEASE_STOP_SELECTED_ACTION_MESSAGE = "pleaseStopSelectedAction";
        public const string KEY_NO_INTERNET_MESSAGE = "noInternet";
        public const string KEY_REMOTE_AVAILABLE_ON_MESSAGE = "remoteAvailableOn";
        public const string KEY_RESTART_IN_ADMIN_MESSAGE = "restartInAdmin";
        public const string KEY_NOT_CONVERTABLE_TO_NUMBER_EXCEPTION_MESSAGE = "notConvertableToNumberExepctionMessage";
        public const string KEY_ONE_IP_SELECTED_NEED_MESSAGE = "oneIpSelectedNeedMessage";
    }
}
