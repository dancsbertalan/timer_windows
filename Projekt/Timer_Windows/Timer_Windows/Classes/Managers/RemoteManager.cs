﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows;
using Timer_Windows.Classes.Exceptions;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// A távirányító kezeléséért felelős osztály.
    /// </summary>
    class RemoteManager
    {
        #region KONSTRUKTOROK
        /// <summary>
        /// Inicializálja azon változókat melyeket már ilyenkor kell.
        /// </summary>
        private RemoteManager()
        {
            userUi = UserUi.GetInstance();
        }
        #endregion

        #region METÓDUSOK

        /// <summary>
        /// Vissza adja mindig ugyan azt a példányt.
        /// </summary>
        /// <returns type="RemoteManager">instance</returns>
        public static RemoteManager GetInstance()
        {
            if (instance == null)
            {
                instance = new RemoteManager();
            }
            return instance;
        }

        /// <summary>
        /// A szerver elindításáért felelős.
        /// </summary>
        public void StartServer()
        {
            try
            {
                userUi.SetLocalIp();
                httpListener.Start();
                httpListener.Prefixes.Add(string.Format("http://{0}:{1}/", userUi.LocalIp, Consts.SERVER_PORT));
                userUi.User.Settings.OnNotifyPropertyChanged("LocalIp");
                ThreadPool.QueueUserWorkItem(o =>
                {
                    try
                    {
                        //MessageBox.Show(string.Format(StringsResxManager.GetRemoteAvailableOn(), userUi.LocalIp));
                        userUi.User.Settings.RemoteIsOn = true;
                        while (httpListener.IsListening)
                        {
                            ThreadPool.QueueUserWorkItem(c =>
                            {
                                var ctx = c as HttpListenerContext;
                                byte[] buffer = null;
                                Dictionary<string, object> response = new Dictionary<string, object>();
                                try
                                {
                                    if (ctx == null)
                                    {
                                        return;
                                    }

                                    string request = ctx.Request.RawUrl.Split('/')[1].Split('?')[0];
                                    Dictionary<string, string> parameters = new Dictionary<string, string>();

                                    if (RequestConsts.getRequests.Contains(request) || RequestConsts.postRequests.Contains(request))
                                    {
                                        //TODO: Body vizsgálata
                                        string body = new StreamReader(ctx.Request.InputStream).ReadToEnd();
                                        if (body.Length > 0)
                                        {
                                            parameters = CreateParametersDictionary(body);
                                        }

                                        if (ctx.Request.HttpMethod == Consts.KEY_GET_ARRAY_NAME)
                                        {
                                            response[FieldConsts.KEY_DATA_FIELD] = GetMethodProcessor(request, parameters);
                                        }
                                        else if (ctx.Request.HttpMethod == Consts.KEY_POST_ARRAY_NAME)
                                        {
                                            response[FieldConsts.KEY_DATA_FIELD] = PostMethodProcessor(request, parameters);
                                        }
                                    }
                                    else
                                    {
                                        response[FieldConsts.KEY_DATA_FIELD] = CreateRequestDoesNotExistResponse(request);
                                    }


                                }
                                catch (KeyNotFoundException knfe)
                                {
                                    Dictionary<string, object> dataField = new Dictionary<string, object>();
                                    dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ONE_OR_MORE_PARAMETERS_ARE_MISSING_MESSAGE);
                                    dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ONE_OR_MORE_PARAMETERS_ARE_MISSING_CODE);
                                    response[FieldConsts.KEY_DATA_FIELD] = dataField;
                                }
                                catch (InvalidBodyFormException ibfe)
                                {
                                    //TODO: 1 - Nem értelmezhető body formátum!
                                    response[FieldConsts.KEY_DATA_FIELD] = ibfe.ResponseFormat;
                                }
                                finally
                                {
                                    if (ctx != null)
                                    {
                                        buffer = Encoding.UTF8.GetBytes(Consts.JsonObjectSerialize(response));
                                        ctx.Response.ContentLength64 = buffer.Length;
                                        ctx.Response.OutputStream.Write(buffer, 0, buffer.Length);

                                        ctx.Response.OutputStream.Close();
                                    }
                                }
                            }, httpListener.GetContext());
                        }
                    }
                    catch (Exception e)
                    {
                        if (userUi.User.Settings.RemoteIsOn)
                        {
                            throw e;
                        }
                    }
                });
            }
            catch (HttpListenerException e)
            {
                if (e.ErrorCode == Consts.HTTP_PERMISSION_DENIED)
                {
                    MessageBox.Show(StringsResxManager.GetRestartInAdmin());
                    StopServer();
                }
            }
            catch (WebException webE)
            {
                userUi.User.Settings.RemoteIsOn = false;
                MessageBox.Show(StringsResxManager.GetNoInternet());
            }
        }

        /// <summary>
        /// A szerver leállítása. Innentől kezdve nem fogjuk tudni elérni a programot remote-al.
        /// </summary>
        public void StopServer()
        {
            httpListener.Stop();
            httpListener.Prefixes.Clear();
            userUi.UnSetLocalIp();
            userUi.User.Settings.RemoteIsOn = false;
        }

        #region REQUEST METÓDUSOK
        #region GET
        /// <summary>
        /// Az actual_request alá tartozó válaszokat készíti el.
        /// </summary>
        /// <returns type="Dictionary<string,object>">dataField</returns>
        private Dictionary<string, object> ActualActionRequest()
        {
            //byte[] buffer = null;
            Dictionary<string, object> dataField = new Dictionary<string, object>();
            dataField.Add(FieldConsts.KEY_HOUR_FIELD, userUi.RemainingTime.Split(':')[0]);
            dataField.Add(FieldConsts.KEY_MINUTE_FIELD, userUi.RemainingTime.Split(':')[1]);

            if (userUi.ActualAction == StringsResxManager.GetNoAction())
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_NO_ACTUAL_ACTION_MESSAGE);
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_NO_CODE);
            }
            else if (userUi.ActualAction != null)
            {
                if (userUi.ActualAction == StringsResxManager.GetShutdownButton())
                {
                    dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_SHUTDOWN_MESSAGE);
                    dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_SHUTDOWN_CODE);
                }
                else if (userUi.ActualAction == StringsResxManager.GetLockButton())
                {
                    dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_LOCK_MESSAGE);
                    dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_LOCK_CODE);
                }
                else if (userUi.ActualAction == StringsResxManager.GetLogoutButton())
                {
                    dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_LOGOUT_MESSAGE);
                    dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_LOGOUT_CODE);
                }
                else if (userUi.ActualAction == StringsResxManager.GetRebootButton())
                {
                    dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_REBOOT_MESSAGE);
                    dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_REBOOT_CODE);
                }
                else if (userUi.ActualAction == StringsResxManager.GetSleepButton())
                {
                    dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_SLEEP_MESSAGE);
                    dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_SLEEP_CODE);
                }
                else if (userUi.ActualAction == StringsResxManager.GetHibernateButton())
                {
                    dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_HIBERNATE_MESSAGE);
                    dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_HIBERNATE_CODE);
                }
            }

            return dataField;
        }
        #endregion

        #region POST
        /// <summary>
        /// Elkészíti azt a response-t mely a set_timer alá tartozik.
        /// </summary>
        /// <param name="parameters">A feldolgozandó paraméeterek. (code,hour,minute) </param>
        /// <returns type="Dictionary<string, object>"></returns>
        public Dictionary<string, object> SetTimerRequest(Dictionary<string, string> parameters)
        {
            int parameterCode = 0;
            Dictionary<string, object> dataField = new Dictionary<string, object>();
            try
            {
                dataField = CodeValidator(parameters[FieldConsts.KEY_CODE_FIELD], out parameterCode, RequestConsts.KEY_SET_TIMER_POST_REQUEST);
                if (dataField.Keys.Count == 0)
                {
                    if (userUi.ActualAction == StringsResxManager.GetNoAction())
                    {
                        TimerManager.StartTimer(parameters[FieldConsts.KEY_HOUR_FIELD], parameters[FieldConsts.KEY_MINUTE_FIELD], true);

                        if (parameterCode == CodeConsts.KEY_ACTUAL_ACTION_SHUTDOWN_CODE)
                        {
                            userUi.ActualAction = StringsResxManager.GetShutdownButton();
                        }
                        else if (parameterCode == CodeConsts.KEY_ACTUAL_ACTION_SLEEP_CODE)
                        {
                            userUi.ActualAction = StringsResxManager.GetSleepButton();
                        }
                        else if (parameterCode == CodeConsts.KEY_ACTUAL_ACTION_REBOOT_CODE)
                        {
                            userUi.ActualAction = StringsResxManager.GetRebootButton();
                        }
                        else if (parameterCode == CodeConsts.KEY_ACTUAL_ACTION_LOGOUT_CODE)
                        {
                            userUi.ActualAction = StringsResxManager.GetLogoutButton();
                        }
                        else if (parameterCode == CodeConsts.KEY_ACTUAL_ACTION_LOCK_CODE)
                        {
                            userUi.ActualAction = StringsResxManager.GetLockButton();
                        }
                        else if (parameterCode == CodeConsts.KEY_ACTUAL_ACTION_HIBERNATE_CODE)
                        {
                            userUi.ActualAction = StringsResxManager.GetHibernateButton();
                        }
                        dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_SUCCESS_MESSAGE);
                        dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_SUCCESS_CODE);
                    }
                    else
                    {
                        dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_MESSAGE);
                        dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_YOUR_SELECTED_ACTION_STARTING_IS_FAILED_CODE);
                    }
                }
            }
            catch (NotConvertableToNumberException nctne)
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, nctne.Message);
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_REQUEST_FIELD_VALUE_IS_NOT_CONVERTABLE_TO_NUMBER_CODE);
            }
            catch (RequestCodeIsNotInterpetable rcini)
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, rcini.Message);
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_REQUEST_CODE_IS_NOT_INTERPETABLE_CODE);
            }

            return dataField;
        }

        /// <summary>
        /// Elkészíti azt a response-t mely az actual_action alá tartozik.
        /// </summary>
        /// <returns type="Dictionary<string, object>"></returns>
        private Dictionary<string, object> StopActualActionRequest()
        {
            Dictionary<string, object> dataField = new Dictionary<string, object>();

            if (userUi.ActualAction != StringsResxManager.GetNoAction())
            {
                TimerManager.StopTimer();
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_MESSAGE);
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_SUCCESS_CODE);
            }
            else
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, MessageConsts.KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_MESSAGE);
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_ACTUAL_ACTION_STOPPING_IS_FAILED_CODE);
            }

            return dataField;
        }
        #endregion

        #endregion

        /// <summary>
        /// A GET tömb alá tartozó kérések feldolgozása itt történik.
        /// </summary>
        /// <param name="request">Az adott request neve</param>
        /// <param name="parameters">A paraméterek</param>
        /// <returns type="Dictionary<string, object>"></returns>
        private Dictionary<string, object> GetMethodProcessor(string request, Dictionary<string, string> parameters)
        {
            //TODO: 1 - Megcsinálni a paraméterek dictionary feltöltésehez a lehető leguniverzálisabb eljárást.
            Dictionary<string, object> dataField = new Dictionary<string, object>();

            if (RequestConsts.getRequests.Contains(request))
            {
                if (request == RequestConsts.KEY_ACTUAL_ACTION_GET_REQUEST)
                {
                    dataField = ActualActionRequest();
                }
            }
            else
            {
                dataField = CreateRequestDoesNotExistResponse(request, Consts.KEY_GET_ARRAY_NAME);
            }

            return dataField;
        }

        /// <summary>
        /// A POST tömb alá tartozó kérések feldolgozása itt történik.
        /// </summary>
        /// <param name="request">Az adott request neve</param>
        /// <param name="parameters">A paraméterek</param>
        /// <returns type="Dictionary<string, object>"></returns>
        private Dictionary<string, object> PostMethodProcessor(string request, Dictionary<string, string> parameters)
        {
            Dictionary<string, object> dataField = new Dictionary<string, object>();

            if (RequestConsts.postRequests.Contains(request))
            {
                if (request == RequestConsts.KEY_SET_TIMER_POST_REQUEST)
                {
                    dataField = SetTimerRequest(parameters);
                }
                else if (request == RequestConsts.KEY_STOP_ACTUAL_ACTION_POST_REQUEST)
                {
                    dataField = StopActualActionRequest();
                }
            }
            else
            {
                dataField = CreateRequestDoesNotExistResponse(request, Consts.KEY_POST_ARRAY_NAME);
            }

            return dataField;
        }

        #region SEGÍTŐ METÓDUSOK
        /// <summary>
        /// Le validálja az adott requestre a kódot. És vissza is adja azt.
        ///     
        ///     Kivételek:
        ///         RequestCodeIsNotInterpetable
        /// 
        /// </summary>
        /// <param name="codeStr">A code string formában</param>
        /// <param name="parameterCode">Az a változó amelybe majd int formában szeretnénk megkapni a codeStr code értékét.</param>
        /// <param name="request">A request neve.</param>
        /// <return type="Dictionary<string,object>">Alapértelmezetten üres marad de ha a codeStr változó alá tartozó érték nem konvertálható akkor elkészíti azt a response-t mely ezt közli így az már nem marad üres.</returns>
        private Dictionary<string, object> CodeValidator(string codeStr, out int parameterCode, string request)
        {
            Dictionary<string, object> dataField = new Dictionary<string, object>();
            parameterCode = 0;
            try
            {
                Consts.numberConverter(codeStr, out parameterCode, FieldConsts.KEY_CODE_FIELD, request);
                if (request == RequestConsts.KEY_SET_TIMER_POST_REQUEST)
                {
                    if (!CodeConsts.validSetTimerCodes.Contains(parameterCode))
                    {
                        throw new RequestCodeIsNotInterpetable(request, parameterCode);
                    }
                }
            }
            catch (NotConvertableToNumberException nctne)
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, nctne.Message);
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_REQUEST_CODE_FIELD_VALUE_IS_NOT_CONVERTABLE_TO_NUMBER_CODE);
            }
            return dataField;
        }

        /// <summary>
        /// A body alapján elkészíti abból a dictionary-t amellyel már könyebben tudunk dolgozni. - GSON
        /// </summary>
        /// <param name="body">Paramétereket tartalmazó body string formában.</param>
        /// <returns type="Dictionary<string,string>">parameters</returns>
        private Dictionary<string, string> CreateParametersDictionary(string body)
        {

            Dictionary<string, string> parameters = new Dictionary<string, string>();
            string[] tempParams;
            try
            {
                tempParams = body.Trim(new char[] { '{', '}' }).Split(',');
                for (int i = 0; i < tempParams.Length; i++)
                {
                    string key = tempParams[i].Split(':')[0].Split('"')[1].Split('"')[0];
                    string value = tempParams[i].Split(':')[1];
                    parameters.Add(key, value);
                }

            }
            catch (IndexOutOfRangeException)
            {
                try
                {
                    //kulcs - érték számít egy elemnek
                    tempParams = body.Split('&');
                    for (int i = 0; i < tempParams.Length; i++)
                    {
                        string key = tempParams[i].Split('=')[0];
                        string value = tempParams[i].Split('=')[1];
                        parameters.Add(key, value);
                    }
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidBodyFormException(body);
                }
            }


            return parameters;
        }

        /// <summary>
        /// Elkészíti azt az responset mely azt mondja, hogy az adott request nem létezik. Három kimenetele lehet:
        /// 1. GET tömbben nem létezik ilyen kérés - "xy_request_does_not_exist_in_post_array"
        /// 2. POST tömbben nem létezik ilyen kérés - "xy_request_does_not_exist_in_get_array"
        /// 3. Egyátalán nem létezik ilyen kérés -  "xy_request_does_not_exist"
        /// </summary>
        /// <param name="nonExistentRequest">A nemlétező request neve.</param>
        /// <param name="arrayName">Azt a http tömb neve amelyben nem létezik a nonExistetRequest változó tartalma. Amennyiben GET/POST kontextusban volt vizsgálva csak akkor van rá szükség.</param>
        /// <returns type="Dictionary<string,object>">doesNotExistResponse</returns>
        private Dictionary<string, object> CreateRequestDoesNotExistResponse(string nonExistentRequest, string arrayName = "")
        {
            Dictionary<string, object> dataField = new Dictionary<string, object>();

            if (arrayName == Consts.KEY_GET_ARRAY_NAME)
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, String.Format(MessageConsts.KEY_REQUEST_DOES_NOT_EXIST_IN_ARRAY_MESSAGE, nonExistentRequest, arrayName.ToLower()));
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_REQUEST_DOES_NOT_EXIST_IN_GET_ARRAY_CODE);
            }
            else if (arrayName == Consts.KEY_POST_ARRAY_NAME)
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, String.Format(MessageConsts.KEY_REQUEST_DOES_NOT_EXIST_IN_ARRAY_MESSAGE, nonExistentRequest, arrayName.ToLower()));
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_REQUEST_DOES_NOT_EXIST_IN_POST_ARRAY_CODE);
            }
            else
            {
                dataField.Add(FieldConsts.KEY_MESSAGE_FIELD, String.Format(MessageConsts.KEY_REQUEST_DOES_NOT_EXIST_MESSAGE, nonExistentRequest));
                dataField.Add(FieldConsts.KEY_CODE_FIELD, CodeConsts.KEY_REQUEST_DOES_NOT_EXIST_CODE);
            }

            return dataField;
        }
        #endregion

        #endregion

        #region MEZŐK
        private static RemoteManager instance;
        private readonly HttpListener httpListener = new HttpListener();
        private UserUi userUi;
        #endregion
    }
}
