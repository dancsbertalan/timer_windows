﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// Az ablakok kezeléssért felelős osztály
    /// </summary>
    public static class WindowManager
    {
        #region METÓDUSOK
        public static int ShowIpSelectWindow(List<string> ipItems)
        {
            App.Current.MainWindow.Hide();
            ipSelectWindow = new IpSelectingWindow(ipItems);

            int selectedIpIndex = 1;
            if (ipSelectWindow.ShowDialog() == false)
            {
                selectedIpIndex = ipSelectWindow.SelectedIp - 1;
            }

            ipSelectWindow.Close();
            App.Current.MainWindow.Show();
            ipSelectWindow = null;
            return selectedIpIndex;
        }
        #endregion

        #region MEZŐK
        private static IpSelectingWindow ipSelectWindow;
        #endregion
    }
}
