﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace Timer_Windows.Classes
{
    public static class TimerManager
    {
        #region "KONSTRUKTOROK"
        /// <summary>
        /// A legfontosabb metódus.
        /// Az alkalmazás megnyitásakor "rögtön" futtatandó annak értekében, hogy a timer a mainthread-en jöjjön létre és ott tudjon dolgozni akár a ThreadPoolban is.
        /// </summary>
        public static void Initialize()
        {
            actionExecuteTimer = new DispatcherTimer();
            actionExecuteTimer.Tick += new EventHandler(actionExecuteTimer_Tick);
            actionExecuteTimer.Interval = new TimeSpan(0, 1, 0);
        }
        #endregion

        #region MEZŐK
        private static DispatcherTimer actionExecuteTimer;
        /// <summary>
        /// A hátralevő idő melyet percben tárolunk el.
        /// </summary>
        private static int duration;
        #endregion

        #region METÓDUSOK
        /// <summary>
        /// Ez fut le minden egyes "kattanásnál". Ha már lejárt az idő akkor futtatja a kiválasztott metódust és a timert leállítja.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void actionExecuteTimer_Tick(object sender, EventArgs e)
        {
            duration--;
            SetUserRemainingTime();
            if (duration == 0)
            {
                //TODO: Felhasználó által kiválasztott műveletet végrehajtatni.
                actionExecuteTimer.Stop();
                PCManager.ExecuteSelectedAction();
            }
        }

        /// <summary>
        /// Minden esetben két számjegyű legyen egy adott szám.
        /// </summary>
        /// <param name="number">Két számjegyűvé alakítandó szám</param>
        /// <returns type="string">retVal</returns>
        private static string AlwaysTwoNumberTime(int number)
        {
            string retVal = number.ToString();
            if (number.ToString().Length < 2)
            {
                retVal = "0" + number;
            }

            return retVal;
        }

        /// <summary>
        ///     Kivételek:
        ///         NotConvertableToNumberException
        /// </summary>
        /// <param name="hourString">Az órák string formában</param>
        /// <param name="minuteString">A percek string formában</param>
        /// <param name="isStartFromRemote">Távirányító által hívta-e meg avagy sem</param>
        /// <returns type="int" name="duration">A teljes duration értéke percekben.</returns>
        private static int GetDuration(string hourString, string minuteString, bool isStartFromRemote)
        {
            //TODO: Validáció!
            int hour = 0;
            int minute = 0;
            try
            {
                if (isStartFromRemote)
                {
                    Consts.numberConverter(hourString, out hour, nameof(hour), RequestConsts.KEY_SET_TIMER_POST_REQUEST);
                    Consts.numberConverter(minuteString, out minute, nameof(minute), RequestConsts.KEY_SET_TIMER_POST_REQUEST);
                }
                else
                {
                    Consts.numberConverter(hourString, out hour, nameof(hour));
                    Consts.numberConverter(minuteString, out minute, nameof(minute));
                }
            }
            catch (NotConvertableToNumberException nctne)
            {
                throw nctne;
            }
            int duration = hour * 60 + minute;

            return duration;
        }

        /// <summary>
        /// Beállítja a usernek a hátralevő idejét az adott művelet végrehajtásáig.
        /// </summary>
        private static void SetUserRemainingTime()
        {
            int minute = duration % 60;
            int hour = (duration - minute) / 60;
            string remainingTime = AlwaysTwoNumberTime(hour) + ":" + AlwaysTwoNumberTime(minute);
            UserUi.GetInstance().RemainingTime = remainingTime;

        }

        /// <summary>
        ///     Kivételek:
        ///         NotConvertableToNumberException
        /// </summary>
        /// <param name="hour">Az órák string formában</param>
        /// <param name="minute">A percek string formában</param>
        /// <param name="isStartFromRemote">Távirányító által hívta-e meg avagy sem</param>
        public static void StartTimer(string hour, string minute, bool isStartFromRemote)
        {
            try
            {
                duration = GetDuration(hour, minute, isStartFromRemote);
                SetUserRemainingTime();
                if (duration < 1)
                {
                    PCManager.ExecuteSelectedAction();
                }
                else
                {
                    actionExecuteTimer.Start();
                }
            }
            catch (NotConvertableToNumberException nctne)
            {
                if (isStartFromRemote)
                {
                    throw nctne;
                }
                MessageBox.Show(nctne.Message);
            }
        }

        /// <summary>
        /// Leállítja a timert. A user actual action mezője innentől kezdve a "no action" értéket fogja tartalmazni a megfelelő nyelven.
        /// </summary>
        public static void StopTimer()
        {
            duration = 0;
            SetUserRemainingTime();
            UserUi.GetInstance().ActualAction = StringsResxManager.GetNoAction();
            actionExecuteTimer.Stop();
        }
        #endregion



    }
}
