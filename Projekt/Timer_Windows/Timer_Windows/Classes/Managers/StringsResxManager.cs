﻿using System.Resources;
using Timer_Windows.Resources.Strings;
using System.Threading;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// A lokalizációnak manager osztálya mellyel a kódban is eltudjuk érni a stringeket.
    /// </summary>
    public static class StringsResxManager
    {
        #region KONSTRUKTOROK
        /// <summary>
        /// Az első használat előtt lefutó static konstruktor.
        /// </summary>
        static StringsResxManager()
        {
            resManager = new ResourceManager(StringsConsts.KEY_STRINGS, typeof(strings).Assembly);
        }
        #endregion

        #region MEZŐK
        private static ResourceManager resManager;
        #endregion

        #region METÓDUSOK
        /// <summary>
        /// Vissza kapjuk a "nincs kiválasztott művelet" megfelelően fordított megfelelőjét az adott nyelvi kulturától függően.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetNoAction()
        {
            return resManager.GetString(StringsConsts.KEY_NO_ACTION_TEXT_BLOCK, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "nincs kiválasztott távirányító" megfelelően fordított megfelelőjét az adott nyelvi kulturától függően.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetNoRemote()
        {
            return resManager.GetString(StringsConsts.KEY_NO_REMOTEABLE_TEXT_BLOCK, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "lezárás" gomb feliratát a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetLockButton()
        {
            return resManager.GetString(StringsConsts.KEY_LOCK_BUTTON, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "kijelentkezés" gomb feliratát a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetLogoutButton()
        {
            return resManager.GetString(StringsConsts.KEY_LOGOUT_BUTTON, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "ujraindítás" gomb feliratát a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetRebootButton()
        {
            return resManager.GetString(StringsConsts.KEY_REBOOT_BUTTON, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "kikapcsolás" gomb feliratát a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetShutdownButton()
        {
            return resManager.GetString(StringsConsts.KEY_SHUTDOWN_BUTTON, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "alvás" gomb feliratát a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns>String</returns>
        public static string GetSleepButton()
        {
            return resManager.GetString(StringsConsts.KEY_SLEEP_BUTTON, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "hibernálás" gomb feliratát a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns>String</returns>
        public static string GetHibernateButton()
        {
            return resManager.GetString(StringsConsts.KEY_HIBERNATE_BUTTON, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza kapjuk a "kérlek állítsd le az aktuális művletet..." szöveget a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetPleaseStopSelectedAction()
        {
            return resManager.GetString(StringsConsts.KEY_PLEASE_STOP_SELECTED_ACTION_MESSAGE, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza adja a "nincs internet kérlek próbáld meg újra amikor van" üzenetet a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetNoInternet()
        {
            return resManager.GetString(StringsConsts.KEY_NO_INTERNET_MESSAGE, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza adja a "A távirányító elérhető az alábbi címen: {0}" üzenetet a megfelelő nyelvi kultúrához viszonyítva. (Paraméterezés nem itt történik!)
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetRemoteAvailableOn()
        {
            return resManager.GetString(StringsConsts.KEY_REMOTE_AVAILABLE_ON_MESSAGE, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza adja a "Kérlek indítsd újra az alkalmazást mint rendszergazda, ha ezt a funkciót akarod használni!" üzenetet a megfelelő nyelvi kultúrához viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetRestartInAdmin()
        {
            return resManager.GetString(StringsConsts.KEY_RESTART_IN_ADMIN_MESSAGE, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza adja a "A {0} mező értéke {1} nem szám!" üzenetet a megfelelő nyelvi kultúrához viszonyítva. (Paraméterezés ott történik ahova a visszatér a metódus!)
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetNotConvertableToNumberExepctionMessage()
        {
            return resManager.GetString(StringsConsts.KEY_NOT_CONVERTABLE_TO_NUMBER_EXCEPTION_MESSAGE, Thread.CurrentThread.CurrentCulture);
        }

        /// <summary>
        /// Vissza adja a "Egy IP címet válassz ki ahhoz hogy tovább mehess!" üzenetet a megfelelő nyelvi kultúrhoz viszonyítva.
        /// </summary>
        /// <returns type="string"></returns>
        public static string GetOneIpSelectedNeedMessage()
        {
            return resManager.GetString(StringsConsts.KEY_ONE_IP_SELECTED_NEED_MESSAGE, Thread.CurrentThread.CurrentCulture);
        }
        #endregion
    }
}
