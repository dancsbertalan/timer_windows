﻿using System.Diagnostics;
using System.Windows;

namespace Timer_Windows.Classes
{
    /// <summary>
    /// Ez az osztály tartalmazza azon metódusokat melyek a gépünkkel szükséges műveleteket végrehajtja
    ///     1. Kikapcsolás
    ///     2. Újraindítás
    ///     3. Alvás
    ///     4. Lezárás
    ///     5. Kijelentkezés
    /// </summary>
    public static class PCManager
    {
        #region MEZŐK
        static ProcessStartInfo processStartInfo;
        #endregion

        #region PROPERTYK

        #endregion

        /// <summary>
        /// A számítógép kikapcsolását végrehajtó metódus
        /// </summary>
        private static void Shutdown()
        {
            processStartInfo = new ProcessStartInfo("shutdown.exe", "-s");
            Process.Start(processStartInfo);
            //MessageBox.Show("Kikapcsolva");
        }

        /// <summary>
        /// A számítógép újraindítását végrehajtó metódus
        /// </summary>
        private static void Reboot()
        {
            processStartInfo = new ProcessStartInfo("shutdown.exe", "-r");
            Process.Start(processStartInfo);
            //MessageBox.Show("Újraindítva");
        }

        /// <summary>
        /// A számítógép altatását végrehajtó metódus
        /// </summary>
        private static void Sleep()
        {
            processStartInfo = new ProcessStartInfo("Rundll32.exe", "powrprof.dll, SetSuspendState 0,1,0");
            Process.Start(processStartInfo);
            //MessageBox.Show("Altatva");
        }

        /// <summary>
        /// A számítógép lezárását végrehajtó metódus
        /// </summary>
        private static void Lock()
        {
            processStartInfo = new ProcessStartInfo("Rundll32.exe", "User32.dll, LockWorkStation");
            Process.Start(processStartInfo);
            //MessageBox.Show("Lezárva");
        }

        /// <summary>
        /// A számítógép kijelentkeztetését végrehajtó metódus
        /// </summary>
        private static void LogOut()
        {
            processStartInfo = new ProcessStartInfo("shutdown.exe", "-l");
            Process.Start(processStartInfo);
            //MessageBox.Show("Kijelentkezve");
        }

        private static void Hibernate()
        {
            processStartInfo = new ProcessStartInfo("shutdown.exe", "-h");
            Process.Start(processStartInfo);
            //MessageBox.Show("Hibernálva");
        }

        /// <summary>
        /// A felhasználó által kiválasztott műveletet végrehajtaja.
        /// </summary>
        public static void ExecuteSelectedAction()
        {
            UserUi u = UserUi.GetInstance();
            if (u.ActualAction == StringsResxManager.GetLockButton())
            {
                Lock();
            }
            else if (u.ActualAction == StringsResxManager.GetLogoutButton())
            {
                LogOut();
            }
            else if (u.ActualAction == StringsResxManager.GetSleepButton())
            {
                Sleep();
            }
            else if (u.ActualAction == StringsResxManager.GetRebootButton())
            {
                Reboot();
            }
            else if (u.ActualAction == StringsResxManager.GetShutdownButton())
            {
                Shutdown();
            }
            else if (u.ActualAction == StringsResxManager.GetHibernateButton())
            {
                Hibernate();
            }
            UserUi.GetInstance().ActualAction = StringsResxManager.GetNoAction();
        }
    }
}
