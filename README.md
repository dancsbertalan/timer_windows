Olvass el!
==========

 

**Timer windowsra**
===============

 

Ennek a szoftvernek az a célja, hogy időzítva tudjuk a számítógépünket “kezelni”. 
Ez alatt az értendő, hogy kikapcsolni/lezárni/kijelentkezni/újraindítani és természetesen lealtatni a gépünket. 
Az utóbbi funkcióhoz a felhasználónak is szükség lesz egy parancs futtatására. (Csak egyszer).  
Ezt a szoftvert csak rendszergazdai jogosultsággal lehet használni.
Tartalmazni fogja ez a szoftver a “streaming” funkciót. Ha ezt bekapcsoljuk
akkor telefonon keresztül (Android) képesek leszünk vezérelni a szoftvert így
állítani, hogy mikor melyik műveletet hajtsa végre a számítógép.

(Ehhez létrehozzuk későbbiekben a projekt repository-t így az is teljesen
elérhető lesz)

Alapértelmezett nyelv az angol de lehetőség lesz további nyelvekre is (elsőnek a
magyar - mivel fejlesztők is azok).

 

## **Felhasznált erőforrások:** ##

-   Minden képet a Gimp 2 szoftverrel készítettem el:
    https://www.gimp.org/

-   (Dinamikus) Nyelviesítéshez:
    https://www.codeproject.com/Articles/35159/WPF-Localization-Using-RESX-Files
    
-   Beállítások tárolásához használt adatbázis kezelő megoldás: (Sqlite)
    https://system.data.sqlite.org/index.html/doc/trunk/www/index.wiki

-   A program ikonja innen való:
    http://simpleicon.com/power-symbol-6.html

-   A "törlés" ikon készítéséhez felhasznált ecset készlet:
    https://www.brusheezy.com/shapes/17595-maux-arrows-shapes

-   .ico kiterjesztésre történő konvertálásban segített:
    http://convertico.com/